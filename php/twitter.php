<?php
Class Twitter {

	function getBearerToken() {
		$bearer_token_creds = base64_encode(TWITTER_API_KEY . ':' . TWITTER_API_SECRET);
		$opts = array(
		  'http'=>array(
		    'method' => 'POST',
		    'header' => 'Authorization: Basic ' . $bearer_token_creds . "\r\n" .
		               'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
		    'content' => 'grant_type=client_credentials'
		  )
		);
		$context = stream_context_create($opts);
		$json = file_get_contents(TWITTER_API_BASE . 'oauth2/token', false, $context);
		$results = json_decode($json, true);

		if($results['token_type'] === "bearer") {
			$_SESSION['bearer_token'] = $results['access_token'];
			return $results['access_token'];
		} else {
			return false;
		}
		return $result;
	}

	function makeRequest($url) {
		if(!isset($_SESSION['bearer_token']) || !$_SESSION['bearer_token']) {
			$token = self::getBearerToken();
		} else {
			$token = $_SESSION['bearer_token'];
		}
		if($token) {
			$opts = array(
			  'http'=>array(
			    'method' => 'GET',
			    'header' => 'Authorization: Bearer ' . $token
			  )
			);
			$context = stream_context_create($opts);
			$json = file_get_contents(TWITTER_API_BASE . $url, false, $context);
			return $json;
		} 
	}
}
?>