<!-- header -->
<div id="banner">
	<div class="container">
		<div id="banner-name"><span>L</span>ocal <span>C</span>oncerts</div>
		<span id="banner-tagline">Finding concerts from around the world to your backyard</span>
	</div>
</div>
<!-- custom options -->
<form method="post" action="" id="banner-options">
	<p id="filter-collapse-text">Event Filters <span class="flaticon-arrows-1 dropdown-arrow event-filter-arrow"></span></p>
	<div class="container" id="banner-options-container">
		<!-- genre -->
		<label class="banner-option">
			<p>Genre: </p>
			<div class="banner-dropdown banner-input-div">
				<span class="current-banner-filter" name="genre">All</span>
				<span class="flaticon-arrows-1 dropdown-arrow"></span>
			</div>
			<ul class="banner-dropdown-menu">
				<li class="active-dropdown-li">
					<span class="dropdown-select-icon flaticon-speaker"></span>
					<span>All</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Classical</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Country</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Electronic</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Folk</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Hip-Hop/Rap</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Jazz</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Pop</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>R&B</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Rock</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>World</span>
				</li>
			</ul>
		</label>
		<!-- location -->
		<label class="banner-option">
			<p>Location: </p>
			<input id="user-location" name="location" placeholder="Location" class="banner-inputs banner-input-div">
		</label>
		<!-- radius -->
		<label class="banner-option">
			<p>Radius: </p>
			<div class="banner-dropdown banner-input-div">
				<span class="current-banner-filter" name="radius">250+ mi</span>
				<span class="flaticon-arrows-1 dropdown-arrow"></span>
			</div>
			<ul class="banner-dropdown-menu">
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>50 mi</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>100 mi</span>
				</li>
				<li class="active-dropdown-li">
					<span class="dropdown-select-icon flaticon-speaker"></span>
					<span>250+ mi</span>
				</li>
			</ul>
		</label>
		<!-- sort -->
		<label class="banner-option">
			<p>Sort: </p>
			<div class="banner-dropdown banner-input-div">
				<span class="current-banner-filter" name="sort">Popular</span>
				<span class="flaticon-arrows-1 dropdown-arrow"></span>
			</div>
			<ul class="banner-dropdown-menu">
				<li class="active-dropdown-li">
					<span class="dropdown-select-icon flaticon-speaker"></span>
					<span>Popular</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Upcoming</span>
				</li>
				<li>
					<span class="dropdown-select-icon flaticon-speaker-1"></span>
					<span>Alphabetical</span>
				</li>
			</ul>
		</label>
	</div>
</form>
<div id="loading-div">
	<img src="assets/images/audio.svg" id="loading-svg">
</div>
<!-- main body -->
<div class="container" id="event-container">
	<div class="row some-gutters">
		<!-- main column -->
		<div class="row some-gutters" id="main-column">
			<div class="col-md-9">
				<!-- main event template -->
				<div class="main-event-div event-div-all" id="main-event-div">
					<script id="main-event-template" type="x-tmpl-mustache">
						<div class="current-carousel-event">
							<span class="main-event-id">{{id}}</span>
							<div class="main-pic-holder">
								<img src="{{image}}" class="event-pic">
							</div>
							{{{artistName}}}
							<p class="sub-event-location">@ 
								{{#_embedded.venues}}
									<span class="event-link" data-venue-id="{{id}}">{{name}}</span>, {{city.name}} {{state.stateCode}}
								{{/_embedded.venues}}
							</p>
							<p class="main-event-date">{{dates.start.localDate}}</p>
						</div>
					</script>
					<div id="main-event-arrows">
						<span class="flaticon-arrows-2 main-arrows" id="left-main-arrow"></span>
						<span class="flaticon-arrows main-arrows"></span>
					</div>
					<div id="main-event-target"></div>
				</div>
				<!-- sub events -->
				<script id="sub-event-template" type="x-tmpl-mustache">
					<div class="sub-event-div event-div-all sub-side-event">
						<div class="event-info">
							<div class="sub-pic-holder">
								<img src="{{image}}" class="event-pic">
							</div>
							{{{artistName}}}
							<p class="sub-featured-artists">
								{{{featuring}}}
							</p>
							<p class="sub-event-location">@ 
								{{#_embedded.venues}}
									<span class="event-link" data-venue-id="{{id}}">{{name}}</span>, {{city.name}} {{state.stateCode}}
								{{/_embedded.venues}}
							</p>
							<p class="sub-event-date">{{dates.start.localDate}}</p>
						</div>
						<a href="{{url}}" target="_blank" class="theme-button full-button">Get Tickets</a>
					</div>
				</script>
				<div class="row some-gutters" id="sub-event-row">
					<div class="col-sm-6" id="left-sub-column">
					</div>
					<div class="col-sm-6" id="right-sub-column">
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<!-- side event template -->
				<script id="side-event-template" type="x-tmpl-mustache">
					<div class="side-event-div event-div-all sub-side-event">
						<div class="event-info">
							<div class="side-pic-holder">
								<img src="{{image}}" class="event-pic">
							</div>
							{{{artistName}}}
							<p class="sub-featured-artists">
								{{{featuring}}}
							</p>
							<p class="sub-event-location">@ 
								{{#_embedded.venues}}
									<span class="event-link" data-venue-id="{{id}}">{{name}}</span>, {{city.name}} {{state.stateCode}}
								{{/_embedded.venues}}
							</p>
							<p class="sub-event-date">{{dates.start.localDate}}</p>
						</div>
						<a href="{{url}}" target="_blank" class="theme-button full-button">Get Tickets</a>
					</div>
				</script>
				<div id="side-event-target"></div>
			</div>
			<div class="show-more" id="home-show-more">
				<p class="show-more-text">load more</p>
				<img src="assets/images/tail-spin.svg" class="show-more-loading">
			</div>
		</div>
	</div>
</div>
<!-- artist/venue banner template -->
<script id="banner-artist-template" type="x-tmpl-mustache">
	<div class="show-banner-container">
		<span id="back-to-home"><span class="flaticon-arrows-3 back-to-home-icon"></span> back to home</span>
		<p class="{{flaticon}} show-banner-flaticon"></p>
		<p>{{name}}</p>
	</div>
</script>
<!-- artist/venue show page -->
<div class="container" id="show-container">
	<div class="row some-gutters">
		<div class="col-md-7">
			<script id="artist-venue-event-template" type="x-tmpl-mustache">
				<div class="artist-venue-event sub-side-event">
					<div class="show-event-pic-holder">
						<img src="{{image}}" class="event-pic">
					</div>
					<div class="show-event-info">
						{{{artistName}}}
						<p class="sub-featured-artists">
									{{{featuring}}}
								</p>
						<p class="sub-event-location">@ 
							{{#_embedded.venues}}
								<span class="event-link" data-venue-id="{{id}}">{{name}}</span>, {{city.name}} {{state.stateCode}}
							{{/_embedded.venues}}
						</p>
						<p class="sub-event-date">{{dates.start.localDate}}</p>
					</div>
					<a href="{{url}}" target="_blank" class="theme-button show-tickets-absolute">Get Tickets</a>
					<a href="{{url}}" target="_blank" class="theme-button full-button show-tickets-normal">Get Tickets</a>
				</div>
			</script>
			<div class="show-column-header">
				<span class="flaticon-map"></span><p>Upcoming Shows</p>
			</div>
			<div id="artist-venue-event-target"></div>
			<div class="show-more" id="show-show-more">
				<p class="show-more-text">load more</p>
				<img src="assets/images/tail-spin.svg" class="show-more-loading">
			</div>
		</div>
		<div class="col-md-5">
			<!-- venue info -->
			<div class="show-column-header" id="venue-info-header">
				<span class="flaticon-round-info-button"></span><p>Venue Info</p>
			</div>
			<div id="venue-info-target"></div>
			<script id="venue-info-template" type="x-tmpl-mustache">
				<div class="venue-div">
					<div class="venue-info-div">
						<span class="flaticon-map-marker venue-info-icon"></span>
						<div>{{address.line1}}, {{city.name}} {{state.stateCode}} {{postalCode}}</div>
					</div>
					<div class="venue-info-div">
						<span class="flaticon-orientation venue-info-icon"></span>
						<a href="https://www.google.com/maps/dir/Current+Location/{{location.latitude}},{{location.longitude}}" target="_blank">Get Directions</a>
					</div>
					{{{parkingDetail}}}
					{{{accessibleSeatingDetail}}}
				</div>
			</script>
			<!-- tweet target/template -->
			<div class="show-column-header" id="twitter-show-header">
				<span class="flaticon-twitter-black-shape"></span><p>Twitter Feed</p>
			</div>
			<div id="tweet-target"></div>
			<script id="tweet-template" type="x-tmpl-mustache">
				<div class="tweet-div">
					<img src="{{user.profile_image_url}}" class="tweet-user-pic">
					<div class="tweet-main-column">
						<div class="tweet-info">
							<span>{{user.name}}</span>
							<span>@{{user.screen_name}}</span>
							<span> - {{created_at}}</span>
						</div>
						<div>{{{text}}}</div>
						<div class="tweet-pic-holder">
							{{{picture}}}
						</div>
					</div>
				</div>
			</script>
		</div>
	</div>
</div>
<!-- no events found -->
<script id="error-template" type="x-tmpl-mustache">
	<div>
		<h3>{{message}}</h3>
		<p>{{subMessage}}</p>
	</div>
</script>
<!-- footer -->
<div class="container" id="footer">
	<div id="footer-border">
		<span class="flaticon-sound-frequency" id="footer-icon"></span>
	</div>
	<div class="footer-div" id="left-footer">
		<span>&copy; Connor Handy, 2017</span>
	</div>
	<div class="footer-div" id="right-footer">
		<a href="http://www.connorhandy.com" target="_blank">Contact</a>
	</div>
</div>