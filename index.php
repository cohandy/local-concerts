<!DOCTYPE html>
<html>
<head>
	<title>Local Concerts</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Bitter:400,700" rel="stylesheet"> 
	<link rel="stylesheet" href="assets/css/css_all.css">
	<link rel="icon" type="image/jpg" href="assets/images/favicon.png">
	<script
	src="https://code.jquery.com/jquery-3.1.1.min.js"
	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	crossorigin="anonymous"></script>
</head>
<body>
	<?php include "views/home.php"; ?>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/mustache.min.js"></script>
	<script src="assets/js/scripts.js"></script>
</body>
</html>