//-------------------------\\
//-------------------------\\
// TicketMaster API Module
//-------------------------\\
//-------------------------\\
var ticketMaster = (function() {
	function fetchEvents(tmUrl, clear) {
		$.ajax({
			type: "GET",
			url: tmUrl,
			async: true,
			dataType: "json",
			success: function(resp) {
				if(clear) { template.clear(); }
				bannerOptions.callback(resp);
			},
			error: function(xhr, status, err) {
				bannerOptions.callback([]);
			}
		});
	}
	function fetchArtistVenueEvents(tmUrl, type, first) {
		$.ajax({
			type: "GET",
			url: tmUrl,
			async: true,
			dataType: "json",
			success: function(resp) {
				if(first) {
					pageTransition.callback(resp, type);
				} else {
					pageTransition.loadMoreCallback(resp);
				}
			},
			error: function(xhr, status, err) {
				pageTransition.callback([], type);
			}
		});
	}
	return {
		fetch: fetchEvents,
		fetchArtistVenue: fetchArtistVenueEvents
	}
})();
//-------------------------\\
//-------------------------\\
// Google Maps Geocoding API Module
//-------------------------\\
//-------------------------\\
var geocode = (function() {
	function getCoordsFromCity(city) {
		$.ajax({
			type: "GET",
			url: constructUrl(),
			async:true,
			dataType: 'json',
			success: function(resp) {
				if(resp.results.length !== 0) {
					var geo = resp.results[0];
					//set new location
					bannerOptions.setLocation(geo.geometry.location.lat + "," + geo.geometry.location.lng);
					//render new events
					bannerOptions.startEvents();
				}
			}
		});
		function constructUrl() {
			var cityForUrl = city.split(" ").join("+");
			var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + cityForUrl + "&key=AIzaSyBsy76L4-oqC1nWxvvUS1BwFHD9fV8PChA";
			return url;
		}
	}
	return {
		getCoords: getCoordsFromCity
	}
})();
//-------------------------\\
//-------------------------\\
// Loading Div Module
//-------------------------\\
//-------------------------\\
var loadingDiv = (function() {
	var fixedLoading = $("#loading-div");
	
	function toggleLoader(visible) {
		if(visible === "show") {
			fixedLoading.fadeIn();
		} else {
			fixedLoading.fadeOut();
		}
	}
	return {
		toggle: toggleLoader
	}
})();
//-------------------------\\
//-------------------------\\
// Banner Options (event options) Module
//-------------------------\\
//-------------------------\\
var bannerOptions = (function() {

	//dom elements
	var bannerForm = $("#banner-options");
	var bannerInputs = $(".banner-input-div");
	var bannerDivs = $(".banner-option");
	var dropdowns = $(".banner-dropdown");
	var collapsedFilter = $("#filter-collapse-text");
	var locationInput = $("#user-location");
	var homeShowMore = $("#home-show-more");
	var currentLocation = "";
	//get users current lat/long if geolocation set
	if(localStorage.getItem("location")) {
		currentLocation = localStorage.getItem("location");
		startEvents();
	} else if(navigator.geolocation) {
		//if geolocation is neither accepted or denied, nothing will happen, this timeout is a fail safe
		var timeout = setTimeout(function() { startEvents(); }, 5000);
		navigator.geolocation.getCurrentPosition(function(pos) {
			clearTimeout(timeout);
			locationInput.val("My Location");
			currentLocation = pos.coords.latitude + "," + pos.coords.longitude;
			localStorage.setItem("location", currentLocation);
			startEvents();
		}, function() {
			startEvents();
		});
	} else {
		startEvents();
	}

	//event listeners
	bannerDivs.click(bannerClick);
	$(document).on('click', hideDropdowns);
	collapsedFilter.click(collapsedDropdown);
	locationInput.blur(getUserLocation);
	bannerForm.submit(getUserLocation);

	//-------------------------\\
	// Start Events Functions
	//-------------------------\\
	function startEvents() {
		//if mobile collapse event filter dropdown
		if(window.innerWidth <= 1000) {
			$("#filter-collapse-text").find('.dropdown-arrow').removeClass('active-dropdown-arrow');
			$("#banner-options-container").removeClass('active-options-container');
		}
		//fetch events (will be rendered after ajax callback)
		ticketMaster.fetch(createEventUrl(0), true);
		loadingDiv.toggle("show");
	}
	function loadMoreEvents(pageNum) {
		$(".current-carousel-event").first().remove();
		ticketMaster.fetch(createEventUrl(pageNum), false);
	}
	function createEventUrl(pageNum) {
		//get all value inputs
		var inputObj = getInputValues();
		//construct url for ticketMaster module
		var tmUrl = "https://app.ticketmaster.com/discovery/v2/events.json?classificationName=" + 
					inputObj.genre + "&" + inputObj.location + "radius=" + inputObj.radius + 
					"&unit=miles&size=24&sort=" + inputObj.sort + "&page=" + pageNum + "&apikey=Ysv1lt7u7byamCAFcka4RjN4wGJcL0N8";
		return tmUrl;
		//loop through all values in form, parse value, return obj with correct values
		function getInputValues() {
			var inputObj = {};
			bannerInputs.each(function() {
				var currentFilter = $(this).find('.current-banner-filter');
				var currentFilterName = currentFilter.attr('name');
				switch(currentFilterName) {
					case "genre":
						var text = $.trim(currentFilter.text());
						if(text === "All") { text = "music"; }
						inputObj.genre = text;
						break;
					case "radius":
						inputObj.radius = parseInt(currentFilter.text());
						break;
					case "sort":
						var text = $.trim(currentFilter.text().toLowerCase());
						var sortFilter = text;
						if(text === "upcoming") {
							sortFilter = "date,asc";
						} else if(text === "popular") {
							sortFilter = "relevance,desc";
						} else {
							sortFilter = "name,asc";
						}
						inputObj.sort = sortFilter;
						break;
				}
			});
			//set location using currentLocation
			if(currentLocation) {
				inputObj.location = "latlong=" + currentLocation + "&";
			} else inputObj.location = "";
			return inputObj;
		}
	}
	function setCurrentLocation(coords) {
		currentLocation = coords;
	}
	function getUserLocation(e) {
		locationInput.blur();
		e.preventDefault();
		var input = locationInput.val();
		if(input !== "My Location") {
			//get lat long from google
			geocode.getCoords(input); 
			//after ajax success geocode module will set new currentLocation and run startEvents
		}
	}
	function tmCallback(resp) {
		//render found events
		if(resp.page.totalElements > 0) {
			template.render(resp._embedded.events);
			//give to main event carousel
			mainEvent.setEvents(resp._embedded.events);
		} else {
			template.none();
		}
		loadingDiv.toggle("hide");
		//remove show more loader
		homeShowMore.children(".show-more-text").show();
		homeShowMore.children(".show-more-loading").hide();
	}

	//-------------------------\\
	// Banner Input Functions
	//-------------------------\\
	//handles event delegation for all banner inputs
	function bannerClick(e) {
		var banner = $(this);
		//find which type of input is inside bannerDiv, activate their specific function
		if(banner.children('.banner-dropdown').length) {
			var drop = dropdownDivClick.bind(this);
			drop(e);	
		} 
	}
	function dropdownDivClick(bannerEvent) {
		//check if clicking on dropdown or menu
		var $eventParent = $(bannerEvent.target).parent();
		if($eventParent.prop('tagName') === "LI" || $eventParent.hasClass('banner-dropdown-menu')) {
			//menu click
			var menu = $(this).find('.banner-dropdown-menu');
			//get li element clicked
			if($eventParent.prop('tagName') === "LI") {
				var li = $eventParent;
			} else {
				var li = $(bannerEvent.target);
			}
			//deselect currently selected
			menu.find('.active-dropdown-li').removeClass('active-dropdown-li').children('.dropdown-select-icon').removeClass("flaticon-speaker").addClass("flaticon-speaker-1");
			//change icon
			li.children('.dropdown-select-icon').removeClass("flaticon-speaker-1").addClass("flaticon-speaker");
			//change li color
			li.addClass("active-dropdown-li");
			//change chosen filter
			menu.prev().children(".current-banner-filter").text(li.last().text());
			startEvents();
		} else {
			//activate dropdown
			bannerEvent.stopPropagation();
			var menu = $(this).find('.banner-dropdown-menu');
			if(!menu.hasClass('active-dropdown-menu')) {
				hideDropdowns();
			}
			menu.toggleClass('active-dropdown-menu');
			$(this).find('.dropdown-arrow').toggleClass('active-dropdown-arrow');
		}
	}
	function hideDropdowns() {
		dropdowns.each(function() {
			var menu = $(this).next();
			var arrow = $(this).children(".dropdown-arrow");
			//hide menu
			menu.removeClass('active-dropdown-menu');
			//unrotate arrow
			arrow.removeClass('active-dropdown-arrow');
		});
	}
	function collapsedDropdown() {
		var container = $(this).next();
		$(this).find('.dropdown-arrow').toggleClass('active-dropdown-arrow');
		container.toggleClass('active-options-container');
	}
	return {
		setLocation: setCurrentLocation,
		startEvents: startEvents,
		callback: tmCallback,
		loadMore: loadMoreEvents
	}
})();
//-------------------------\\
//-------------------------\\
// Template Module
//-------------------------\\
//-------------------------\\
var template = (function() {
	var mainEventDiv = $("#main-event-div");
	var showMoreDiv = $("#home-show-more");
	//templates
	var mainEventTemplate = $("#main-event-template").html();
	var subEventTemplate = $("#sub-event-template").html();
	var sideEventTemplate = $("#side-event-template").html();
	var noneFoundTemplate = $("#error-template").html();
	//parse all templates
	Mustache.parse(mainEventTemplate);
	Mustache.parse(subEventTemplate);
	Mustache.parse(sideEventTemplate);
	//fetch targets
	var mainEventTarget = $("#main-event-target");
	var subEventTargetLeft = $("#left-sub-column");
	var subEventTargetRight = $("#right-sub-column");
	var sideEventTarget = $("#side-event-target");

	function renderEvents(eventArr) {
		mainEventDiv.show();
		//render main event
		var mainEventObj = jQuery.extend({}, eventArr[eventArr.length - 1]);
		var mainEvent = prepEvent(mainEventObj, "main");
		var mainHTML = $(Mustache.render(mainEventTemplate, mainEvent));
		mainEventTarget.append(mainHTML);
		mainHTML.height();
		mainHTML.addClass('active-current-carousel-event');

		var length = eventArr.length;
		var mainDivHeight = $(".main-event-div").first().height();
		for(var i=0;i<length;i++) {
			//get heights of all three target areas
			var subLeftHeight = subEventTargetLeft.height() + mainDivHeight;
			var subRightHeight = subEventTargetRight.height() + mainDivHeight;
			var sideHeight = sideEventTarget.height();
			//set default template
			var template = subEventTemplate;
			//find shortest column and append event
			if(subLeftHeight < sideHeight && subRightHeight < sideHeight) {
				if(subLeftHeight < subRightHeight) {
					var event = prepEvent(eventArr[i], "sub");
					var target = subEventTargetLeft;
				} else {
					var event = prepEvent(eventArr[i], "sub");
					var target = subEventTargetRight;
				}
			} else {
				var event = prepEvent(eventArr[i], "side");
				var target = sideEventTarget;
				template = sideEventTemplate;
			}
			//append to shortest column
			var newEvent = $(Mustache.render(template, event));
			target.append(newEvent);
			//must force reflow for proper transition after append
			newEvent.height();
			newEvent.css('opacity', '1');
		}
		//determine if there is more events to laod
		if(eventArr.length >= 24) {
			showMoreDiv.show();
		} else showMoreDiv.hide();
	}
	function prepEvent(obj, template) {
		//find proper image
		obj.image = findPic(obj.images, template);
		if(template === "main" || !obj._embedded.hasOwnProperty('attractions') || !obj._embedded.attractions[0].hasOwnProperty('name')) {
			//if no attractions just display name of event
			obj.artistName = headlineHTML(obj.name, "venue", obj._embedded.venues[0].id);
		//if guests then combine into string, skipping headliner
		} else if(obj._embedded.attractions.length > 1) {
			var feats = [];
			for(var i=1;i<obj._embedded.attractions.length;i++) {
				feats.push('<span class="event-link" data-artist-id="' + obj._embedded.attractions[i].id + '">' + obj._embedded.attractions[i].name + '</span>');
			}
			obj.featuring = "with: " + feats.join(",  ");
		}
		//get headlining artists name
		if(!obj.hasOwnProperty('artistName')) {
			obj.artistName = headlineHTML(obj._embedded.attractions[0].name, "artist", obj._embedded.attractions[0].id);
		}
		return obj;
		function headlineHTML(objName, type, typeId) {
			//assign correct data attribute
			if(type === "venue") {
				var dataAttr = 'data-venue-id';
			} else {
				var dataAttr = 'data-artist-id';
			}
			if(template === "main") {
				return '<h3 class="main-event-name" ' + dataAttr + '="' + typeId + '">' + objName + '</h3>';
			} else {
				return '<h5 ' + dataAttr + '="' + typeId + '">' + objName + '</h5>';
			}
		}
	}
	function findPic(images, template) {
		if(template === "main") {
			var res = 600
		} else if(template === "sub") {
			var res = 350;
		} else {
			var res = 250;
		}
		//loop through images in obj and find pic with proper res
		var length = images.length;
		var pic = null;
		var backupPic = null;
		for(var i=0;i<length;i++) {
			if(images[i].ratio === "16_9" && images[i].width >= res && images[i].width <= res * 2) {
				pic = images[i].url;
				break;
			} else if(images[i].width >= res - 50 && images[i].width <= res * 2) {
				backupPic = images[i].url;
			}
		}
		//if no pics matched reqs, use backup or first in arr
		if(!pic) { 
			!backupPic ? pic = images[0].url : pic = backupPic;
		}
		return pic;
	}
	function clearEvents() {
		mainEventTarget.html("");
		subEventTargetLeft.html("");
		subEventTargetRight.html("");
		sideEventTarget.html("");
	}
	function getMainEvent(obj) {
		var nextObj = prepEvent(obj, "main");
		return $(Mustache.render(mainEventTemplate, nextObj));
	}
	function noneFound() {
		showMoreDiv.hide();
		mainEventDiv.hide();
		subEventTargetLeft.append(Mustache.render(noneFoundTemplate, {"message": "No events found", "subMessage": "Try increasing the radius for more results"}));
	}
	return {
		render: renderEvents,
		clear: clearEvents,
		getMainEvent: getMainEvent,
		findPic: findPic,
		prepEventObj: prepEvent,
		none: noneFound
	}
})();
//-------------------------\\
//-------------------------\\
// Main Event Module
//-------------------------\\
//-------------------------\\
var mainEvent = (function() {
	//dom elements/module vars
	var arrows = $(".main-arrows");
	var currentEvents = [];
	var carouselInterval = null;
	var mainEventDiv = $(".main-event-div").first();
	var mainEventTarget = $("#main-event-target");
	var currentTransition = null;

	//events
	arrows.click(manualSlide);

	//carousel functions
	function startMainCarousel() {
		if(carouselInterval) { clearInterval(carouselInterval); }
		carouselInterval = setInterval(function() {
			carouselShift("left");
		}, 5000);
	}
	function manualSlide(e) {
		//get direction
		var direction = "right";
		if(e.target.getAttribute('id') === "left-main-arrow") {
			direction = "left";
		}
		clearInterval(carouselInterval);
		//if animation is still occuring stop the current setTimeout, remove previous main event
		if(currentTransition) {
			clearTimeout(currentTransition);
			$('.current-carousel-event').first().remove();
		}
		//shift carousel to next event
		carouselShift(direction);
	}
	function carouselShift(direction) {
		var currentMainEvent = $('.current-carousel-event').first();
		//find index of current image and then find next image (automatic carousel moves backwards)
		var index = findCurrentEventIndex(), nextIndex = findNextIndex(index, direction);
	 	var nextEvent = template.getMainEvent(currentEvents[nextIndex]);
	 	mainEventTarget.append(nextEvent);
	 	//must force reflow to allow for a proper transition right after being appended
	 	nextEvent.height();
	 	nextEvent.addClass('active-current-carousel-event');
	 	currentMainEvent.removeClass('active-current-carousel-event');
	 	//get rid of previous event after it finishes transition
	 	currentTransition = setTimeout(function() { currentMainEvent.remove();currentTransition = null; }, 750);
	}
	function findCurrentEventIndex() {
		var id = mainEventDiv.find('.main-event-id').text();
		var length = currentEvents.length;
		var index = null;
		for(var i=0;i<length;i++) {
			if(id === currentEvents[i].id) {
				index = i;
				break;
			}
		}
		return index;
	}
	function findNextIndex(currentIndex, direction) {
		var eventsLength = currentEvents.length;
		if(direction === "right") {
			var nextIndex = currentIndex + 1;
			if(nextIndex <= eventsLength - 1) {
				return nextIndex;
			} else {
				return 0;
			}
		} else {
			var nextIndex = currentIndex - 1;
			if(nextIndex >= 0) {
				return nextIndex;
			} else {
				return eventsLength - 1;
			}
		}
	}

	//setter functions
	function setCurrentEvents(events) {
		currentEvents = events;
		//restart carousel
		startMainCarousel();
	}
	return {
		setEvents: setCurrentEvents
	}
})();

//-------------------------\\
//-------------------------\\
// Page transition module
//-------------------------\\
//-------------------------\\
var pageTransition = (function() {
	var eventContainer = $("#event-container");
	var bannerContainer = $("#banner-options-container");
	var bannerOptions = $("#banner-options");
	var showContainer = $("#show-container");
	var currentShowObj = {"id": null, "type": null};

	//events
	$(document).click(eventClick);
	$(document).on('click', "#back-to-home", homeTransition);

	function eventClick(e) {
		var $target = $(e.target);
		//check if clicking on venue or artist link
		if(typeof $target.attr('data-artist-id') !== 'undefined') {
			var showPageHeadline = $target.text();
			fetchNewEvents($target.attr('data-artist-id'), "artist", showPageHeadline);
			currentShowObj.id = $target.attr('data-artist-id');
			currentShowObj.type = "artist";
		} else if(typeof $target.attr('data-venue-id') !== 'undefined') {
			//if target doesn't have class event-link, target is click on headline, find proper venue name
			if(!$target.hasClass('event-link')) {
				var showPageHeadline = $target.parent().find('.sub-event-location').children('.event-link').text();
			} else {
				var showPageHeadline = $target.text();
			}
			fetchNewEvents($target.attr('data-venue-id'), "venue", showPageHeadline);
			currentShowObj.id = $target.attr('data-venue-id');
			currentShowObj.type = "venue";
		}
	}
	function fetchNewEvents(id, type, bannerHeadline) {
		loadingDiv.toggle("show");
		if(type === "artist") {
			var urlParam = "attractionId=" + id;
		} else {
			var urlParam = "venueId=" + id;
		}
		var tmUrl = "https://app.ticketmaster.com/discovery/v2/events.json?" + urlParam + 
					"&classificationName=music&size=16&sort=date,asc&apikey=Ysv1lt7u7byamCAFcka4RjN4wGJcL0N8";
		scrollToTop();
		//make call to ticket master
		ticketMaster.fetchArtistVenue(tmUrl, type, true);
		if(type !== "venue") {
			//fetch tweets, if venue it will be fetched after tm callback
			twitter.fetch(bannerHeadline, type);
		}
		//swap banner options with show banner
		bannerTransition(type, bannerHeadline);
	}
	function loadMoreEvents(pageNum) {
		if(currentShowObj.type === "artist") {
			var urlParam = "attractionId=" + currentShowObj.id;
		} else {
			var urlParam = "venueId=" + currentShowObj.id;
		}
		var tmUrl = "https://app.ticketmaster.com/discovery/v2/events.json?" + urlParam + 
					"&classificationName=music&size=16&page=" + pageNum + "&sort=date,asc&apikey=Ysv1lt7u7byamCAFcka4RjN4wGJcL0N8";
		//make call to ticket master
		ticketMaster.fetchArtistVenue(tmUrl, currentShowObj.type, false);
	}
	function bannerTransition(type, bannerHeadline) {
		//hide overflow in bannerOptions
		bannerOptions.css('overflow', 'hidden');
		//render new show banner
		var newBanner = showPageTemplate.banner({"name": bannerHeadline}, type);
		//if already on show page
		if($(".show-banner-container").length > 0) {
			var currentShowBanner = $(".show-banner-container").first();
			currentShowBanner.css('top', '-45px');
			setTimeout(function() { currentShowBanner.remove(); }, 650);
		} else {
			//slide up main options banner
			bannerContainer.addClass("hidden-options-container");
		}
		bannerOptions.append(newBanner);
		//slide newBanner up
		newBanner.height();
		newBanner.addClass("active-show-banner-container");
		//update history
		pageHistory.showPage(bannerHeadline);
		//fade out home container with show
		showPageTemplate.clear();
		eventContainer.fadeOut();
		showContainer.fadeIn();
	}
	function eventCallback(resp, type) {
		//render found events
		if(resp.page.totalElements > 0) {
			//render new events
			showPageTemplate.render(resp._embedded.events);
			//render venue info if venue
			if(type === "venue") {
				var venue = resp._embedded.events[0]._embedded.venues[0];
				showPageTemplate.renderInfo(venue);
				//fetch venues tweets if it has social
				if(venue.hasOwnProperty('social') && venue.social.hasOwnProperty('twitter')) {
					twitter.fetch(venue.social.twitter.handle, "venue");
				} else {
					$("#twitter-show-header").hide();
				}
			}
		} else {
			//show no results
			showPageTemplate.none();
		}
		//hide loading div
		loadingDiv.toggle("hide");
	}
	function loadMoreCallback(resp) {
		var showMoreDiv = $("#show-show-more");
		if(resp.hasOwnProperty('_embedded')) {
			showPageTemplate.render(resp._embedded.events);
		} else $("#show-show-more").hide();
		showMoreDiv.children(".show-more-text").show();
		showMoreDiv.children(".show-more-loading").hide();
	}
	function homeTransition() {
		var showBanner = $(".show-banner-container").first();
		//slide down main options banner
		bannerContainer.removeClass("hidden-options-container");
		//slide show banner down
		showBanner.removeClass("active-show-banner-container");
		//fade out event container, fade in show container
		showContainer.fadeOut();
		eventContainer.fadeIn();
		//replace current history state
		pageHistory.home();
		//change overflow in bannerOptions and remove showBanner after animation is done
		setTimeout(function(){ showBanner.remove();bannerOptions.css('overflow', 'visible'); }, 650);
	}
	function scrollToTop() {
		$("html, body").animate({
			scrollTop: "0"
		}, 300);
	}
	return {
		callback: eventCallback,
		loadMore: loadMoreEvents,
		loadMoreCallback: loadMoreCallback,
		homeTransition: homeTransition,
		scrollToTop: scrollToTop
	}
})();

//-------------------------\\
//-------------------------\\
// Show Page Templates
//-------------------------\\
//-------------------------\\
var showPageTemplate = (function() {
	var noneFoundTemplate = $("#error-template").html();
	var bannerTemplate = $("#banner-artist-template").html();
	var eventTemplate = $("#artist-venue-event-template").html();
	var infoTemplate = $("#venue-info-template").html();
	var tweetTemplate = $("#tweet-template").html();
	var tweetTarget = $("#tweet-target");
	var infoTarget = $("#venue-info-target");
	var showTarget = $("#artist-venue-event-target");
	var venueInfoHeader = $("#venue-info-header");
	var showMoreDiv = $("#show-show-more");

	function renderNewBanner(obj, type) {
		var flaticon = null;
		if(type === "artist") {
			flaticon = "flaticon-music";
			venueInfoHeader.hide();
		} else {
			flaticon = "flaticon-buildings";
			venueInfoHeader.show();
		}
		obj.flaticon = flaticon;
		return $(Mustache.render(bannerTemplate, obj));
	}
	function renderEvents(events) {
		var length = events.length;
		for(var i=0;i<length;i++) {
			//prep event obj
			var event = template.prepEventObj(events[i], "sub");
			var eventHTML = $(Mustache.render(eventTemplate, event));
			showTarget.append(eventHTML);
			eventHTML.height();
			eventHTML.css('opacity', '1');
		}
		if(events.length >= 16) {
			showMoreDiv.show();
		} else showMoreDiv.hide();
	}
	function renderVenueInfo(obj) {
		infoTarget.append(Mustache.render(infoTemplate, prepVenueObj(obj)));
		function prepVenueObj(obj) {
			if(obj.hasOwnProperty('parkingDetail')) {
				obj.parkingDetail = '<div class="venue-info-div">' +
										'<span class="flaticon-transport venue-info-icon"></span>' + 
										'<div>' + obj.parkingDetail + '</div>' +
									'</div>';
			}
			if(obj.hasOwnProperty('accessibleSeatingDetail')) {
				obj.accessibleSeatingDetail = '<div class="venue-info-div">' +
											'<span class="flaticon-disabled venue-info-icon"></span>' + 
											'<div>' + obj.accessibleSeatingDetail + '</div>' +
										'</div>';
			}
			return obj;
		}
	}
	function renderTweets(tweets) {
		var length = tweets.length;
		for(var i=0;i<length;i++) {
			var tweetHTML = $(Mustache.render(tweetTemplate, prepTweetObj(tweets[i])));
			tweetTarget.append(tweetHTML);
			tweetHTML.height();
			tweetHTML.css('opacity', '1');
		}
		function prepTweetObj(obj) {
			//make date readable
			obj.created_at = timeSince(new Date(obj.created_at));
			//find pic if any
			if(obj.entities.hasOwnProperty('media')) {
				obj.picture = '<img src="' + obj.entities.media[0].media_url + '" class="tweet-pic">';
			}
			//go through tweet text and make any links/mentions/hashtags active
			obj.text = parseTweetText(obj);
			return obj;
			function timeSince(date) {
				var seconds = Math.floor((new Date() - date) / 1000);
			    var interval = Math.floor(seconds / 31536000);

			    if(interval > 1) {
			        return interval + "y";
			    }
			    interval = Math.floor(seconds / 2592000);
			    if(interval > 1) {
			        return interval + "mo";
			    }
			    interval = Math.floor(seconds / 86400);
			    if(interval > 1) {
			        return interval + "d";
			    }
			    interval = Math.floor(seconds / 3600);
			    if(interval > 1) {
			        return interval + "h";
			    }
			    interval = Math.floor(seconds / 60);
			    if(interval > 1) {
			        return interval + "m";
			    }
			    return Math.floor(seconds) + "s";
			}
		}
		function parseTweetText(tweet) {
			var text = tweet.text,
				parsedLinks = [];
			//go through entities array in tweet and find any links
			for(var prop in tweet.entities) {
				if(tweet.entities[prop].length) {
					parseLink(tweet.entities[prop], getBaseUrl(prop), prop);
				}
			}
			//iterate through parsedLinks and replace old dead links with new html
			var linkLength = parsedLinks.length;
			if(linkLength) {
				for(var i=0;i<linkLength;i++) {
					text = text.replace(new RegExp(parsedLinks[i].text), parsedLinks[i].link);
				}
			}
			return text;
			function parseLink(entityArr, baseUrl, entity) {
				var length = entityArr.length;
				for(var i=0;i<length;i++) {
					//entity has the start and end indices for each link, find the link
					var deadLink = text.substring(entityArr[i].indices[0], entityArr[i].indices[1])
					//give it proper href and html tags
					switch(entity) {
						case "urls":
							var linkHTML = '<a href="' + entityArr[i].expanded_url + '" target="_blank">' + deadLink + '</a>';
							break;
						case "user_mentions":
							var linkHTML = '<a href="' + baseUrl + deadLink + '" target="_blank">' + deadLink + '</a>';
							break;
						case "hashtags":
							var linkHTML = '<a href="' + baseUrl + deadLink.substring(1) + '?src=hash" target="_blank">' + deadLink + '</a>';
							break;
						default:
							var linkHTML = "";
					}
					//push to array that will hold og text and new 'a' element
					parsedLinks.push({"text": deadLink, "link": linkHTML});
				}
			}
			function getBaseUrl(entity) {
				switch(entity) {
					case "hashtags":
						return "https://twitter.com/hashtag/";
						break;
					case "user_mentions":
						return "https://twitter.com/";
						break;
					default:
						return "";
				}
			}
		}
	}
	function noneFound() {
		showTarget.append(Mustache.render(noneFoundTemplate, {"message": "No upcoming shows found", "submessage": ""}));
	}
	function clearEvents() {
		showTarget.html("");
		infoTarget.html("");
		tweetTarget.html("");
	}
	return {
		banner: renderNewBanner,
		render: renderEvents,
		renderInfo: renderVenueInfo,
		renderTweets: renderTweets,
		clear: clearEvents,
		none: noneFound
	}
})();
//-------------------------\\
//-------------------------\\
// Show More Module
//-------------------------\\
//-------------------------\\
var showMore = (function() {
	var showMoreDivs = $(".show-more");

	//events
	showMoreDivs.click(loadMore)

	function loadMore(e) {
		//get show more div if click on text
		if(e.target.tagName === "P") { 
			var target = $(e.target).parent();
		} else var target = $(e.target);
		//get text and loader elements
		target.find(".show-more-text").hide();
		target.find(".show-more-loading").show();
		//check to see if on homepage or show
		if(target.attr('id') === "home-show-more") {
			//get page number by dividing amount received in each request (24) by amount of events
			var eventAmount = $(".event-div-all").length,
				pageNum = Math.floor(eventAmount/24);
			bannerOptions.loadMore(pageNum);
		} else if(target.attr('id') === "show-show-more") {
			var eventAmount = $(".artist-venue-event").length,
				pageNum = Math.floor(eventAmount/16);
			pageTransition.loadMore(pageNum);
		}
	}
})();
//-------------------------\\
//-------------------------\\
// Twitter API Module
//-------------------------\\
//-------------------------\\
var twitter = (function() {
	var twitterHeader = $("#twitter-show-header");

	function fetchTweets(name, type) {
		$.ajax({
			type: "POST",
			url: "ajax/twitter/request.php",
			async: true,
			dataType: 'json',
			data: {"url": constructQuery()},
			success: function(resp) {
				if(resp.statuses.length > 0) {
					twitterHeader.show();
					showPageTemplate.renderTweets(resp.statuses);
				} else twitterHeader.hide();
			},
			error: function(xhr, status, err) {
				twitterHeader.hide();
			}
		});
		function constructQuery() {
			var baseUrl = "1.1/search/tweets.json?q=";
			var endUrl = "%20-filter%3Aretweets%20filter%3Aimages&count=12&lang=en";
			if(type !== "venue") {
				//url encode name
				var nameArr = name.split(" ");
				var nameQuery = "%22" + nameArr.join("%20") + "%22";
				var operators = "concert%20OR%20show%20OR%20tour";
				return baseUrl + nameQuery + "%20" + operators + endUrl;
			} else {
				return baseUrl + "from:" + name + "%20OR%20to:" + name + endUrl;
			}
		}
	}
	return {
		fetch: fetchTweets
	}
})();
//-------------------------\\
//-------------------------\\
// History Module
//-------------------------\\
//-------------------------\\
var pageHistory = (function() {
	//initial push state
	history.pushState({}, "", "#home");

	//events
	window.onpopstate = function(e) {
		if($(".show-banner-container").length > 0) {
			pageTransition.homeTransition();
		} else {
			window.history.go(-2);
		}
	}

	function showPage(title) {
		history.replaceState({}, "", "#show");
		document.title = title + " Concerts";
	}
	function homePage() {
		history.replaceState({}, "", "#home");
		document.title = "Local Concerts";
	}
	return {
		showPage: showPage,
		home: homePage
	}
})();